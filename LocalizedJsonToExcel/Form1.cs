﻿using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace LocalizedJsonToExcel
{
    public partial class Form1 : Form
    {
        private Dictionary<string, string> _dic;
        private DataTable _dataTable;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text))
            {
                MessageBox.Show(this, "Please past some json data", "No Data !!!", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    _dic = JsonConvert.DeserializeObject<Dictionary<string, string>>(textBox1.Text);
                    BuildDataTable();
                }
                catch
                {
                    MessageBox.Show(this, "Can't convert your json to DataTable", "Fail to convert json data", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                    return;
                }


                using (var ms = new MemoryStream())
                {
                    using (var excel = new ExcelPackage())
                    {
                        var ws = excel.Workbook.Worksheets.Add("Data");
                        ws.Cells["A1"].LoadFromDataTable(_dataTable, true);
                        ws.Cells[1, 1, 1, _dataTable.Rows.Count].Style.Font.Bold = true;
                        FormatDateColumns(ws);
                        ws.Cells[ws.Dimension.Address].AutoFitColumns();
                        excel.SaveAs(ms);
                        ms.Position = 0;
                    }
                    using (FileStream file = new FileStream(saveFileDialog1.FileName, FileMode.Create, FileAccess.Write))
                    {
                        ms.WriteTo(file);
                    }
                    MessageBox.Show(this, "Data has been convert and saved in " + saveFileDialog1.FileName,
                        "Data saved successfully !!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
        }

        private void FormatDateColumns(ExcelWorksheet ws)
        {
            if (_dataTable.Rows.Count == 0)
            {
                return;
            }

            var dateColumns = from DataColumn d in _dataTable.Columns
                              where d.DataType == typeof(DateTime)
                              select d.Ordinal + 1;

            foreach (var dc in dateColumns)
            {
                ws.Cells[2, dc, _dataTable.Rows.Count + 1, dc].Style.Numberformat.Format = "yyyy-MM-dd HH:mm:ss";
            }
        }

        private void BuildDataTable()
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add("Key");
            _dataTable.Columns.Add("Language");
            foreach (var key in _dic)
            {
                var row = _dataTable.NewRow();
                row["key"] = key.Key;
                row["Language"] = key.Value;
                _dataTable.Rows.Add(row);
            }
        }
    }
}
